#!/bin/bash
sudo docker-compose -f production.yml build
sudo docker-compose -f production.yml up
docker-compose -f production.yml run --rm django python manage.py makemigrations
docker-compose -f production.yml run --rm django python manage.py migrate
