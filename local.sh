#!/bin/bash
sudo docker-compose -f local.yml build
sudo docker-compose -f local.yml up
sudo docker-compose -f local.yml run --rm django python manage.py makemigrations
sudo docker-compose -f local.yml run --rm django python manage.py migrate
